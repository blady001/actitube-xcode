//
//  ScaleGenerator.cpp
//  acidtube
//
//  Created by Dawid Brzyszcz on 31/03/2019.
//  Copyright © 2019 Dawid Brzyszcz. All rights reserved.
//

#include "ScaleGenerator.hpp"
#include <iostream>

using namespace scales;

ScaleGenerator::ScaleGenerator() {}
ScaleGenerator::~ScaleGenerator() {}

const std::string ScaleGenerator::TONE_NAMES[] = { "C", "C#", "D", "D#", "E", "F", "F#", "G", "G#", "A", "A#", "B"};
const int ScaleGenerator::MIDI_NN[] = { 48, 49, 50, 51, 52, 53, 54, 55, 56, 57, 58, 59 };

int ScaleGenerator::getScaleTonesCount(Scale scale) {
    switch(scale) {
        case chromatic:
            return 13;
        case ionian:
            return 8;
        case aeolian:
            return 8;
    }
}

void ScaleGenerator::computeScaleTones(Tone key, Scale scale, int* output) {
    int startTone = key;
    int tonePosition = startTone;
    
    int* steps;
    int stepsLength = this->getScaleTonesCount(scale);
    
    switch (scale) {
        case ionian: {
            int ionian[] = { 2, 2, 1, 2, 2, 2, 1 };
            steps = ionian;
            break;
        }
        case aeolian: {
            int aeolian[] = { 2, 1, 2, 2, 1, 2, 2 };
            steps = aeolian;
            break;
        }
        default: {
            // chromatic as a fallback
            int chromatic[] = { 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1 };
            steps = chromatic;
            break;
        }
    }
    
    for (int i = 0; i < stepsLength; i++) {
        output[i] = tonePosition % 12; // length of all available notes
        tonePosition += steps[i];
    }
}

void ScaleGenerator::prettyPrintNotes(int* tones, int tonesSize) {
    for (int i = 0; i < tonesSize; i++) {
        std::cout << TONE_NAMES[tones[i]] << std::endl;
    }
}

void ScaleGenerator::convertToMidiCodes(int *tones, int tonesSize) {
    for (int i = 0; i < tonesSize; i++) {
        tones[i] = MIDI_NN[tones[i]];
    }
}

void ScaleGenerator::fillTones(Tone key, Scale scale, int *output) {
    this->computeScaleTones(key, scale, output);
    // this->prettyPrintNotes(output, this->getScaleNoteCount(scale));
    this->convertToMidiCodes(output, this->getScaleTonesCount(scale));
}
