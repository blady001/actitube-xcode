//
//  SequenceDataGenerator.cpp
//  acidtube
//
//  Created by Dawid Brzyszcz on 31/03/2019.
//  Copyright © 2019 Dawid Brzyszcz. All rights reserved.
//

#include "SequenceDataGenerator.hpp"
#include <cstdlib>
#include <math.h>

SequenceDataGenerator::SequenceDataGenerator(int initialDensity, int lowestVelocity, scales::Tone initialKey, scales::Scale initialScale) {
    this->density = initialDensity;
    this->lowestVelocity = lowestVelocity;
    this->scaleGenerator = new scales::ScaleGenerator();
    this->key = initialKey;
    this->scale = initialScale;
    
    this->noteArray = new int[MATRIX_SIZE];
    this->octaveArray = new int*[MATRIX_SIZE];
    this->slideArray = new int*[MATRIX_SIZE];
    this->velocityArray = new int*[MATRIX_SIZE];
    for(int i = 0; i < MATRIX_SIZE; i++) {
        this->octaveArray[i] = new int[MATRIX_SIZE];
        this->slideArray[i] = new int[MATRIX_SIZE];
        this->velocityArray[i] = new int[MATRIX_SIZE];
    }
}

SequenceDataGenerator::~SequenceDataGenerator() {
    delete this->scaleGenerator;
    
    for(int i = 0; i < MATRIX_SIZE; i++) {
        delete[] this->octaveArray[i];
        delete[] this->slideArray[i];
        delete[] this->velocityArray[i];
    }
    delete[] this->octaveArray;
    delete[] this->slideArray;
    delete[] this->velocityArray;
    delete[] this->noteArray;
}

int SequenceDataGenerator::generateRandom(int max) {
    return rand() % max;
}

void SequenceDataGenerator::generateSlides(int** input) {
    for (int row = 0; row < MATRIX_SIZE; row++) {
        // threshold calculation for comparison
        float rowThreshold = (1.0/3) * row + 30.0;
        //std::cout << "Threshold for row #" << row << ": " << rowThreshold << std::endl;
        
        for (int col = 0; col < MATRIX_SIZE; col++) {
            int value = this->generateRandom(100);
            //            std::cout << "Comparing value vs threshold: " << value << " | " << rowThreshold << std::endl;
            input[row][col] = value < rowThreshold ? 1 : 0;
        }
    }
}

void SequenceDataGenerator::generateOctaves(int **input) {
    for (int row = 0; row < MATRIX_SIZE; row++) {
        float rowThreshold = (1.0/3) * row + 10.0;
        
        for (int col = 0; col < MATRIX_SIZE; col++) {
            int n1 = this->generateRandom(100) < rowThreshold ? 1 : 0;
            int n2 = this->generateRandom(100) < rowThreshold ? 1 : 0;
            input[row][col] = n1 + n2;
        }
    }
}


float SequenceDataGenerator::calcNonlinearThreshold(int input) {
    float x0 = 0.0;
    float x1 = 60.0;
    float y0 = 10.0;
    float y1 = 80.0;
    float exp = 1.2;
    
    if (((input - x0)/(x1 - x0)) == 0) {
        return y0;
    }
    else if (((input - x0)/(x1 - x0)) > 0) {
        return y0 + (y1 - y0) * pow(((input - x0)/(x1 - x0)), exp);
    }
    else {
        return y0 + (y1 - y0) * (-1.0) * pow( (((-1) * input) + x0) / (x1 - x0), exp);
    }
}

void SequenceDataGenerator::generateVelocities(int **input) {
    int colGenerationCount = MATRIX_SIZE - 4;
    int rowGenerationCount = MATRIX_SIZE - 3;
    
    for (int row = 0; row < rowGenerationCount; row++) {
        int firstVelocity = this->generateRandom(2) == 0 ? 80 : 127;
        // first value in each column is assigned on the row basis
        input[row][0] = firstVelocity;
        
        float t1 = (17.0/12) * row + 5.0;
        float t2 = this->calcNonlinearThreshold(row);
        
        for (int col = 1; col < colGenerationCount + 1; col++) {
            int n1 = this->generateRandom(100) < t1 ? 1 : 0;
            int n2 = this->generateRandom(100) < t2 ? 1 : 0;
            int sum = n1 + n2;
            int toAssign;
            switch(sum) {
                case 2:
                    toAssign = 127;
                    break;
                case 1:
                    toAssign = this->lowestVelocity;
                    break;
                default:
                    toAssign = 0;
                    break;
            }
            input[row][col] = toAssign;
        }
        
        // fill 3 last values in a row
        input[row][colGenerationCount + 1] = 127;
        input[row][colGenerationCount + 2] = 80;
        input[row][colGenerationCount + 3] = 80;
    }
    
    // fill last 3 rows to be the same
    input[rowGenerationCount] = input[rowGenerationCount - 1];
    input[rowGenerationCount + 1] = input[rowGenerationCount - 1];
    input[rowGenerationCount + 2] = input[rowGenerationCount - 1];
}

void SequenceDataGenerator::generateNotes(int* input) {
    int scaleSize = this->scaleGenerator->getScaleTonesCount(this->scale);
    int availableTones[scaleSize];
    this->scaleGenerator->fillTones(this->key, this->scale, availableTones);
    
    for (int i = 0; i < MATRIX_SIZE; i++) {
        int index = generateRandom(scaleSize);
        input[i] = availableTones[index];
    }
}

void SequenceDataGenerator::generateSequenceData() {
    this->generateOctaves(this->octaveArray);
    this->generateSlides(this->slideArray);
    this->generateVelocities(this->velocityArray);
    this->generateNotes(this->noteArray);
}

void SequenceDataGenerator::getSequenceData(MidiNoteData *input) {
    int* slides = this->slideArray[this->density];
    int* octaves = this->octaveArray[this->density];
    int* velocities = this->velocityArray[this->density];
    
    for (int i = 0; i < MATRIX_SIZE; i++) {
        MidiNoteData midiNoteData;
        midiNoteData.midiNoteCode = this->noteArray[i];
        midiNoteData.octave = octaves[i];
        midiNoteData.slide = slides[i];
        midiNoteData.velocity = velocities[i];
        input[i] = midiNoteData;
    }
}


//
// GETTERS & SETTERS
//
int SequenceDataGenerator::getDensity() {
    return this->density;
}

void SequenceDataGenerator::setDensity(int density) {
    this->density = density;
}

int SequenceDataGenerator::getLowestVelocity() {
    return this->lowestVelocity;
}

void SequenceDataGenerator::setLowestVelocity(int vel) {
    for (int i = 0; i < MATRIX_SIZE; i++) {
        for (int j = 0; j < MATRIX_SIZE; j++) {
            if (this->velocityArray[i][j] == this->lowestVelocity) {
                this->velocityArray[i][j] = vel;
            }
        }
    }
    this->lowestVelocity = vel;
}

void SequenceDataGenerator::setKey(scales::Tone key) {
    this->key = key;
}

scales::Tone SequenceDataGenerator::getKey() {
    return this->key;
}

void SequenceDataGenerator::setScale(scales::Scale scale) {
    this->scale = scale;
}

scales::Scale SequenceDataGenerator::getScale() {
    return this->scale;
}

int** SequenceDataGenerator::getSlideArray() {
    return this->slideArray;
}

int** SequenceDataGenerator::getOctaveArray() {
    return this->octaveArray;
}

int** SequenceDataGenerator::getVelocityArray() {
    return this->velocityArray;
}

int* SequenceDataGenerator::getNoteArray() {
    return this->noteArray;
}
