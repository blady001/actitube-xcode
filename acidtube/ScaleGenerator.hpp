//
//  ScaleGenerator.hpp
//  acidtube
//
//  Created by Dawid Brzyszcz on 31/03/2019.
//  Copyright © 2019 Dawid Brzyszcz. All rights reserved.
//

#ifndef ScaleGenerator_hpp
#define ScaleGenerator_hpp

#include <stdio.h>
#include <string>

#endif /* ScaleGenerator_hpp */

namespace scales {
    enum Scale {
        chromatic,
        ionian,
        aeolian
    };

    enum Tone {
        C,
        Cis,
        D,
        Dis,
        E,
        F,
        Fis,
        G,
        Gis,
        A,
        Ais,
        B
    };

    class ScaleGenerator {
    private:
        const static std::string TONE_NAMES[12];
        const static int MIDI_NN[12];
        
        void computeScaleTones(Tone key, Scale scale, int* output);
        void convertToMidiCodes(int* tones, int tonesSize);
    public:
        ScaleGenerator();
        ~ScaleGenerator();
        int getScaleTonesCount(Scale scale);
        void fillTones(Tone key, Scale scale, int* output);
        void prettyPrintNotes(int* tones, int tonesSize);
    };
}
