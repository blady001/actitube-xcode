//
//  main.cpp
//  acidtube
//
//  Created by Dawid Brzyszcz on 29/03/2019.
//  Copyright © 2019 Dawid Brzyszcz. All rights reserved.
//

#include <iostream>
#include <cstdlib>
#include <ctime>
// #include <math.h>

#include "SequenceDataGenerator.hpp"

int LOWEST_VELOCITY = 80; // TODO: maybe pass it to the function instead of making global??

void printArray(int** arr, int rows, int cols) {
    for (int i = 0; i < rows; i++) {
        for (int j = 0; j < cols; j++) {
            int value = arr[i][j];
            std::cout << value << ",";
        }
        std::cout << std::endl;
    }
}
//
// //////
//

int main(int argc, const char * argv[]) {
    // insert code here...
    std::cout << "Hello, World!\n";

    //
    // TEST ARRAY GENERATION
    //
    srand (time(NULL));
    
    // FINAL TEST OF THE SequenceDataGenerator
    SequenceDataGenerator* sdg = new SequenceDataGenerator(32, 80, scales::Tone::G, scales::Scale::ionian);
    
    MidiNoteData output[MATRIX_SIZE];
    
    sdg->generateSequenceData();
    sdg->getSequenceData(output);
    
    std::cout << "======== GENERATED SLIDES ========" << std::endl;
    printArray(sdg->getSlideArray(), MATRIX_SIZE, MATRIX_SIZE);
    std::cout << "======== GENERATED OCTAVES ========" << std::endl;
    printArray(sdg->getOctaveArray(), MATRIX_SIZE, MATRIX_SIZE);
    std::cout << "======== GENERATED VELOCITIES ========" << std::endl;
    printArray(sdg->getVelocityArray(), MATRIX_SIZE, MATRIX_SIZE);
    std::cout << "======== GENERATED NOTES ========" << std::endl;
    int* notes = sdg->getNoteArray();
    for (int i = 0; i < MATRIX_SIZE; i++)
        std::cout << notes[i] << std::endl;
    
    std::cout << "======== FINAL RESULT ========" << std::endl;
    for (int i = 0; i < MATRIX_SIZE; i++) {
        MidiNoteData m = output[i];
        std::cout << m.midiNoteCode << "," << m.velocity << "," << m.octave << "," << m.slide << std::endl;
    }

    
//    int* arr[MATRIX_SIZE];
//    for(int i = 0; i < MATRIX_SIZE; i++)
//        arr[i] = new int[MATRIX_SIZE];
//
//    SequenceDataGenerator* dg = new SequenceDataGenerator(32, LOWEST_VELOCITY, scales::Tone::A, scales::Scale::aeolian);
//    dg->generateSlides(arr);
//    printArray(arr);
    
//    int scaleSequence[64];
//    dg->generateNotes(scaleSequence);
//
//    for (int i = 0; i < 64; i++) {
//        std::cout << scaleSequence[i] << std::endl;
//    }
    return 0;
}
