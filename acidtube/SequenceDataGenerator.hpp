//
//  SequenceDataGenerator.hpp
//  acidtube
//
//  Created by Dawid Brzyszcz on 31/03/2019.
//  Copyright © 2019 Dawid Brzyszcz. All rights reserved.
//

#ifndef SequenceDataGenerator_hpp
#define SequenceDataGenerator_hpp

#include <stdio.h>
#include "ScaleGenerator.hpp"

#endif /* SequenceDataGenerator_hpp */

struct MidiNoteData {
    int midiNoteCode;
    int velocity;
    int octave;
    int slide;
};

const static int MATRIX_SIZE = 64;

class SequenceDataGenerator {
private:
    int lowestVelocity;
    int density;
    scales::Tone key;
    scales::Scale scale;
    
    scales::ScaleGenerator* scaleGenerator;
    
    int** slideArray;
    int** velocityArray;
    int** octaveArray;
    int* noteArray;
    
    int generateRandom(int max);
    float calcNonlinearThreshold(int input);
    
public:
    SequenceDataGenerator(int initialDensity, int lowestVelocity, scales::Tone initialKey, scales::Scale initialScale);
    ~SequenceDataGenerator();
    
    void generateSlides(int** input);
    void generateVelocities(int** input);
    void generateOctaves(int** input);
    void generateNotes(int* input);
    void generateSequenceData();
    void getSequenceData(MidiNoteData* input);
    
    int getLowestVelocity();
    int getDensity();
    scales::Tone getKey();
    scales::Scale getScale();
    
    void setKey(scales::Tone key);
    void setScale(scales::Scale scale);
    void setLowestVelocity(int vel);
    void setDensity(int density);
    
    // 4 getters below are just for debugging and shouldn't be used
    int** getSlideArray();
    int** getVelocityArray();
    int** getOctaveArray();
    int* getNoteArray();
};
